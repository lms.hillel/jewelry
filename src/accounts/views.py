from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView

from accounts.models import CustomUser

from accounts.forms import RegistrationForm


class UserLogin(LoginView):
    pass


class UserLogout(LogoutView):
    template_name = 'registration/logged_out.html'


class UserRegistration(CreateView):
    form_class = RegistrationForm
    template_name = "registration/registration.html"
    success_url = reverse_lazy('jewelry:index')


class UserProfile(UpdateView):
    model = CustomUser
    fields = ['email', 'username', 'first_name', 'last_name', 'birthday', 'phone_number', 'address']
    template_name = 'profile.html'
    success_url = reverse_lazy('jewelry:index')

from django.urls import path

from accounts.views import UserLogin, UserLogout

from accounts.views import UserRegistration

from accounts.views import UserProfile

app_name = "accounts"


urlpatterns = [
    path("login/", UserLogin.as_view(), name='login'),
    path("logout/", UserLogout.as_view(), name="logout"),
    path("registration/", UserRegistration.as_view(), name="registration"),
    path("profile/<int:pk>", UserProfile.as_view(), name="profile"),

]

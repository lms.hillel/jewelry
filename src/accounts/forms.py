from django.contrib.auth.forms import UserCreationForm
from django.forms import EmailField

from accounts.models import CustomUser


class RegistrationForm(UserCreationForm):
    email = EmailField(max_length=200, help_text="Registration without email is not possible!")

    class Meta:
        model = CustomUser
        fields = ['email', 'username', 'password1', 'password2']

from django.views.generic import TemplateView, ListView, DetailView

from jewelry_shop.models import Product, Category


class Index(TemplateView):
    template_name = 'index.html'
    extra_context = {'categories': Category.objects.all()}


class About(TemplateView):
    template_name = 'about.html'
    extra_context = {'categories': Category.objects.all()}


class Shop(ListView):
    model = Product
    template_name = 'shop.html'
    extra_context = {'categories': Category.objects.all()}


class Contact(TemplateView):
    template_name = 'contact.html'
    extra_context = {'categories': Category.objects.all()}


class CategoryView(ListView):

    model = Product
    template_name = 'shop.html'
    extra_context = {'categories': Category.objects.all()}

    def get_queryset(self):
        category = self.kwargs.get('pk')
        object_list = Product.objects.all().filter(category=category)
        return object_list


class ProductDetails(DetailView):
    model = Product
    template_name = 'shop-single.html'
    extra_context = {'categories': Category.objects.all()}

    def get_object(self, queryset=None):
        obj = Product.objects.get(uuid=self.kwargs.get("uuid"))
        self.extra_context['product'] = obj
        self.extra_context['images'] = obj.product_images.all()

        return obj

from django.utils.translation import ugettext as _
from phonenumber_field.modelfields import PhoneNumberField
from django.db import models

from jewelry_shop.services.utils import generate_uuid


class BaseModel(models.Model):
    class Meta:
        abstract = True

    create_date = models.DateTimeField(null=True, auto_now_add=True)
    last_update = models.DateTimeField(null=True, auto_now=True)


class BonusCard(BaseModel):
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    card_name = models.CharField(_("card name"), max_length=150, blank=True)
    min_amount = models.IntegerField(null=False)
    discount = models.IntegerField(null=False)

    def __str__(self):
        return f"{self.card_name}"


class Category(BaseModel):
    category_name = models.CharField(_("category name"), max_length=150, blank=True)
    image = models.ImageField(upload_to='images/', null=False, blank=True)

    def __str__(self):
        return f"{self.category_name}"


class Product(BaseModel):
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    class METAL_CHOICES(models.IntegerChoices):  # noqa:
        INDEFINED = 0, "indefined"
        SILVER = 1, "Silver"
        GOLD = 2, "Gold"

    prod_name = models.CharField(_("prod name"), max_length=150, blank=True)
    category = models.ForeignKey("jewelry_shop.Category", null=True, on_delete=models.CASCADE)
    material = models.PositiveSmallIntegerField(choices=METAL_CHOICES.choices, default=METAL_CHOICES.INDEFINED)
    fineness = models.IntegerField(null=True, blank=True)
    weight = models.FloatField(null=True, blank=True)
    diameter = models.FloatField(null=True, blank=True)
    description = models.CharField(_("description"), max_length=1024, blank=True)
    price = models.IntegerField(null=True)
    balance = models.IntegerField(null=True, blank=True)
    main_image = models.ImageField(upload_to='images/', null=False, blank=True)

    def __str__(self):
        return f"{self.prod_name}"


class ProductMedia(BaseModel):
    product = models.ForeignKey("jewelry_shop.Product", related_name='product_images',
                                on_delete=models.CASCADE, null=True)
    media_link = models.ImageField(upload_to='images/', null=False, blank=False)


class Order(BaseModel):
    class STATUS_CHOICES(models.IntegerChoices):  # noqa:
        NEW = 0, "New"
        PROCESSED = 1, "Processed"
        PAID = 2, "Paid"
        SENT = 3, "Sent"

    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES.choices, default=STATUS_CHOICES.NEW)
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)

    class PAYMENT_CHOICES(models.IntegerChoices):  # noqa:
        ON_DELIVERY = 0, "Payment on delivery"
        BY_REQUISITES = 1, "By requisites"
        VISA_MASTERCARD = 2, "Visa/Mastercard"

    payment = models.PositiveSmallIntegerField(choices=PAYMENT_CHOICES.choices, default=PAYMENT_CHOICES.BY_REQUISITES)

    user = models.ForeignKey("accounts.CustomUser", null=False, on_delete=models.CASCADE)
    amount = models.FloatField(null=False)

    address = models.CharField(_("address"), max_length=150, blank=False)
    email = models.EmailField(_("email address"), unique=True)
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    phone_number = PhoneNumberField(unique=True, null=True)

    def __str__(self):
        return f"{self.id} {self.status}"


class OrderContent(models.Model):
    order = models.ForeignKey("jewelry_shop.order", null=False, on_delete=models.CASCADE)
    product = models.ForeignKey("jewelry_shop.product", null=False, on_delete=models.CASCADE)
    count = models.IntegerField(null=False, default=1)

    def __str__(self):
        return f"{self.product}"

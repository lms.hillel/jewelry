from datetime import datetime
import requests
from celery import shared_task
from django.core.mail import EmailMessage
from django.conf import settings


@shared_task
def sending_rate():
    date = datetime.now()
    rate = 0
    url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange'
    params = {
        'valcode': 'USD',
        'date': date.strftime("%Y%m%d"),
        'json': ''
    }

    result_raw = requests.get(url, params=params)

    if 300 > result_raw.status_code >= 200:
        parsed_data = result_raw.json()
        for line in parsed_data:
            rate = line.get('rate')
            date = line.get('exchangedate')

        email = EmailMessage(subject='Current dollar rate',
                             body=f'Current dollar rate: date: {date}, '
                                  f'rate: {rate}',
                             to=['mikolaz2727@gmail.com'])
        if settings.DEBUG:
            email.send(fail_silently=False)
        else:
            email.send(fail_silently=True)

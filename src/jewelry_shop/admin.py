from django.contrib import admin

from jewelry_shop.models import Category, Product, ProductMedia, Order, OrderContent, BonusCard

admin.site.register([BonusCard, Category, Product, ProductMedia, Order, OrderContent])

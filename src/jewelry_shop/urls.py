from django.urls import path

from jewelry_shop.views import About, Shop, Contact

from jewelry_shop.views import CategoryView

from jewelry_shop.views import ProductDetails

from jewelry_shop.views import Index

app_name = "jewelry"


urlpatterns = [
    path("", Index.as_view(), name="index"),
    path("about/", About.as_view(), name="about"),
    path("shop/", Shop.as_view(), name="shop"),
    path("contact/", Contact.as_view(), name="contact"),
    path("shop/category/<int:pk>", CategoryView.as_view(), name="category"),
    path("product/<uuid:uuid>", ProductDetails.as_view(), name="details"),
]

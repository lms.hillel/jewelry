from django.views.generic import TemplateView

from jewelry_shop.models import Category


class Room(TemplateView):
    template_name = 'room.html'
    extra_context = {'categories': Category.objects.all(),
                     'room_name': 'chatroom'}

from django.urls import path
from .views import Room

app_name = "chat"

urlpatterns = [
    path('chatroom/', Room.as_view(), name='room'),
]

import os
from jewelry_main.settings.base import *  # noqa:

DEBUG = False
ALLOWED_HOSTS = ['ec2-3-87-206-12.compute-1.amazonaws.com']

MEDIA_ROOT = os.path.join(BASE_DIR, "media/") # noqa
STATIC_ROOT = os.path.join(BASE_DIR, "static/") # noqa
